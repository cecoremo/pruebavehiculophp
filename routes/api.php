<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VehiculosController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
|            tipos de peticiones esta muy ocupoado?
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::get('vehiculos', 'VehiculoController@index');*/
/*Route::get('vehiculos', 'VehiculoController@show');*/

/*Route::get('vehiculos/tipo/{tipo}', 'VehiculoController@index');*/


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {

    return $request->user();
});

Route::get('Listado_Vehiculos', [VehiculosController::class, 'index']);
Route::get('filterType',  [VehiculosController::class, 'searchByTipo']);
Route::get('filterByPLate',  [VehiculosController::class, 'searchByPLate']);
Route::post('Crear', [VehiculosController::class, 'Create']);
Route::put('Actualizar/{PLate}', [VehiculosController::class, 'Update']);
Route::delete('Eliminar/{PLate}', [VehiculosController::class, 'delete']);


