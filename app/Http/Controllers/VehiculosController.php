<?php



namespace App\Http\Controllers;

use App\Models\vehiculos;
use Illuminate\Http\Request;

use Carbon\Carbon;/*FEcha  actual del log*/
use DB;
use Response;
use Illuminate\Support\Facades\Http;
use App;
use Config;
use GuzzleHttp\Psr7\Message;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;/*importar clase log */
use Session;
use Exception;
use Hash;
use Mail;
use DateTime;
use Illuminate\Support\Facades\Config as FacadesConfig;
use JWT;
use Storage;
use Illuminate\Support\Str;

class VehiculosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

       return response()->json(vehiculos::all(), 200);
    }


    public function searchByTipo(Request $request)
    {
        $tipo = $request->input('Type');
        $vehiculos = vehiculos::where('Type', $tipo)->get();
        return response()->json($vehiculos);
    }
    public function searchByPLate(Request $request)
    {
        $tipo = $request->input('PLate');
        $vehiculos = vehiculos::where('PLate', $tipo)->get();
        return response()->json($vehiculos);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     public function Create(Request $request)
     {
         $vehiculos = vehiculos::firstOrNew(['id' => $request->input('id')]);

         if (!$vehiculos->exists) {
             $vehiculos = vehiculos::create($request->all());
             Log::info('Returned information', ['data' => $vehiculos], '.Time: '.Carbon::now());

             return response()->json($vehiculos, 200);

         } else {
             Log::error('THIS RECORD HAS BEEN SAVE ALREADY', ['data' => $vehiculos], '.Time: '.Carbon::now());
             return response()->json(["message"=>"SOMETHING WENT WRONG CHECK THE DATA AND TRY AGAIN"], 400);

            }
     }

        /*UPDATE VEHICULOS*/

        public function Update(Request $request, $PLate)
    {
        $vehiculos = vehiculos::where('PLate', $PLate)->first();
            if(is_null($vehiculos)){
                Log::error('record no found'.'.Time: '.Carbon::now());
                return response()->json(["message"=>"Record no Found"], 404);
            }
            $vehiculos->update($request->all());
            Log::info('Returned information', ['data' => $vehiculos], '.Time: '.Carbon::now());
            return response()->json($vehiculos,200);
    }
    /*DELETE vehiculos*/

    public function delete($PLate)
    {
        $vehiculos = vehiculos::where('Plate', $PLate)->first();
        $current_time = Carbon::now()->toDateTimeString();
            if (is_null($vehiculos)) {
                Log::warning('Vehiculo not found', ['time' => $current_time]);
                return response()->json(["message" => "Vehiculo not found"], 404);
            }
        $vehiculos->delete();
        Log::info('VEhiculo Deleted', ['data' => $vehiculos, 'time' => $current_time]);
        return response()->json(["message" => "Vehiculo Deleted"],200);
    }

}
