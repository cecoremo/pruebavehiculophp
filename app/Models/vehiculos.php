<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class vehiculos extends Model
{
    protected $table = 'vehiculos';
    use HasFactory;
    protected $fillable = ['StatusID' , 'Mark' , 'Model' , 'Type' ,'Price','PLate'
]
;

}
